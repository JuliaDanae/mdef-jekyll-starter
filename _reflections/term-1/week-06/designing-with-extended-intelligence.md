---
title: 6 | Designing with Extended Intelligence
period: 5-11 November 2018
date: 2018-11-11 12:00:00
term: 1
published: true
---

<span class="text-small">*Introduced by Ramon Sangüesa and Lucas Peña*</span>


![]({{site.baseurl}}/AIFlowChart.jpg)
*AI Flow Chart*


This week was used to try challenge and demystify the idea of intelligence, which in turn allowed us to think more wisely of possible future use and development of AI.

### What is intelligence?
In groups, we tried to define the meaning of *intelligence* and came up with different forms of *intelligence*.
A straightforward answer would be to define intelligence as the ability to acknowledge, process, and share information. We could add to this initial definition the ability to learn from experiences and adapt to an environment, this definition would imply intelligence is specific to each context, one’s culture and beliefs. If we take the scientific/biologic definition, it is the response to a stimulus, either internal - such as dehydration - or external - such as a sound.
The greater the capacity to process information the more complex the neural network. The more cross-connections between neurons, the better responsiveness to an environment or situation.

Could the following be different forms of intelligence or are they all combined to form intelligence.
- Emotional intelligence (cognitive)
- Biological intelligence
- Social intelligence (collective)

Is learning a form of intelligence? And in this case could you unlearn intelligence? Is intuition a form of intelligence? Could intuition be understood as the lowest form of intelligence?

A correct definition of intelligence would not be defined as universal, as it is constantly changing within different contexts.
However we could universally think of Intelligence as a muscle, growing if you activate/train it or stopping of growth if not stimulated. Understanding this in this way only would define intelligence as more functional rather than intellectual?

Decision-making is missing in those definition, and is highly linked to intelligence… I will talk further about decision further in the reflection.

Design intelligence / design with intelligence / design for intelligence?
If we look back at the Greek philosophers, Plato and Aristotle, discussing intelligence, we can understand two different approaches.
Aristotle sees intelligence as the something we have because we are given > bottom-up.
Plato sees intelligence as something coming from senses and exploration > top-down (symbols).
Develop and compare those ideas

If we struggle defining the meaning of intelligence, how can we create or define what is or what should be artificial intelligence?


**Symbol grounding problems**
How something is and how to communicate it? Which comes first? (the chicken or the egg idea)
Where does this knowledge come from? We must agree what is information in first place.
Who defines those meanings to objects? Objectively defined but perspectively defined too.
How to establish common ground? Who’s definition to trust?

[Noam Chomsky on human intelligence and evironment:](https://isreview.org/issue/76/human-intelligence-and-environment):
*“It seems that there is something about us, our intelligence, which entails that we’re capable of acting in ways that are rational within a narrow framework but are irrational in terms of other long-term goals”*. No significant action is being taken by humans in response to the current environmental crisis, and these non-existence actions will affect the future generations sharing the remains of this planet. We are are doing things rationally for trom a market point of view, but from an environmental and social point of view is the very opposite.  
Chomsky takes the example of the American biologist, Ernst Mayr, who defines human intelligence as *“a kind of lethal mutation”*, something that will not last long with the development of new advanced intelligence redefining its meaning. When AI will be completely implemented on our planet, it will outdo what we, at the time, thought was the most advanced form of intelligence and might end the life of humans. This is the idea Chomsky shares through Mayr’s definition. If our intelligence life is a *”lethal mutation”*, we can not believe there’s nothing to be done and stay in this stagnation point, waiting to for our fate.

Turing’s Imitation Game (1950) vs. Searle’s Chinese Room (1980)
How do we know what we know and how do we prove what we know is true?
I have looked at Turing’s thought experiment and understand it is missing semiotics, such as empathy and intuition, to measure accurately intelligence. Without semiotics, it only mimicking a superficial part of the human intelligence. The algorithm in this experiment is designed only to answer specific answers. The machine is given the answers to learn before being asked for those.
How can the thought experiment be extended for better assessing intelligence? Turing’s imitation game is focusing on the results rather than the process to how it got to those given answers. For instance, in a mathematic exam, one student decides to cheat and looks at someone else’s, supposedly correct, answer. That answer might be correct on both papers, however it is the one that demonstrated how to get to that answer that will help understand accurately its understanding to the problem. By proving the answer, we can alleviate the trust issues linked with Turing’s method when it comes to making important decisions. How can a machine find the correct answer to a problem when it requires more intuition and empathy, complex multi-choices answers?

### Artificial Intelligence in design

1956’s Dartmouth Artificial Intelligence conference was organised by John McCarthy, a computer scientist and cognitive scientist, one of the founding fathers of the discipline of AI.
This conference or workshop lead to a extended brainstorming on the subject, allowed today a better understanding of the basics of automatic computers but also lead to questions on the subject that could still be asked today.

A automatic computer is an automatic calculator programmed, using speed and memory capacities, to stimulate the machine. How can a computer be programmed to use a language?
Humans’ thoughts consists of manipulating words according to rules of reasoning and rules of conjecture. In that line of thought, forming a generalisation consists of admitting a new word and some rules acquired from shared experience.

Randomness and creativity are part of the design of AI. Randomness must be guided by intuition in order to be efficient and an educated guess needs to be done to control that randomness.
Idea at the time was to create a machine that could control the environment. Norbert Wiener defined cybernetics, in 1948, as "the scientific study of control and communication in the animal and the machine."* Following this idea of self-regulating mechanisms, Wiener discussed the concept of “redundancy”, in the sense of having multiple computing mechanisms operating simultaneously on the same problem, so that errors may be recognised and corrected on their own.

**Process of AI**

![]({{site.baseurl}}/AI Diagram.jpg)

The process of AI follows a few steps:
Collection of data, such as through Internet of Things.
Analysis of those in the form of statistics.
Data visualisation.
Decision of actions to take.
Learning using comparison, correction and new goals.
Agents are created for those kind of systems to achieve the goals. In one environment, dynamic and complex types of agents are playing a role.

Build rational agents? Herbert A. Simon Simon attempted to determine the techniques and/or behavioral processes that a person or organization could bring to bear to achieve approximately the best result given limits on rational decision making. Simon writes: *“The human being striving for rationality and restricted within the limits of his knowledge has developed some working procedures that partially overcome these difficulties. These procedures consist in assuming that he can isolate from the rest of the world a closed system containing a limited number of variables and a limited range of consequences”* (Simon, Herbert (1976), Administrative Behavior (3rd ed.), New York: The Free Press).

Small agents DIAGRAM p.104

**Build AI**

Where does AI live?
Can I trace back where my data is stored, who it is sold to and how it is used?

To build an AI, and analysis of the situation is used to design the purpose through classification of possible responses. First we define an intelligent object/ an agent by defining:
Goals
Types of tasks
Input
Output
Context of operation

A trust could develop between the user and the AI, which would mean the user doesn’t question the AI to give reasons for giving a result.
However, if the decision-making is done within a different context, it might more subject to questioning. For instance, if a judge, with biased ideas, in a courtroom was replaced by an AI to avoid biased ideas, the machine would be highly questioned when taking a decision whether someone is guilty or not. In this case, it is also the social constructs around the idea of *good* and *bad* that is to question.

**AI as meta-object**

A meta object is an object that manipulates, creates, describes, or implements objects, including itself.
*“Meta-objects are examples of the computer science concept of reflection, where a system has access (usually at run time) to its own internal structure. Reflection enables a system to essentially rewrite itself on the fly, to alter its own implementation as it executes.”* (Smith, Brian C (1982-01-01). “Procedural Reflection In Programming Languages”)
In that sense, AI is a meta-object, the realisation that objects and products are part of an extended technical, social, economical, cultural ecosystem. This ecosystem is released as part of an infrastructure.


**Basic type of machine learning**

Supervised learning
Dictates if creating an accurate prediction.

1. Classification
Analyse different types of metrics.
PCA shrinking multi dimensions and K is the algorithm.
Regression
Aims to create a line that can predict the result.
Decision tree

2. Un-supervised
Labels are not created but associated - clustering.
Emergent properties are created.

3. Renforcement
Typically used in autonomous control systems.
A continuous iteration of supervised and/or unsupervised learning in accordance with a goal or belief.

**Neural networks**

Neural networks are analytic and constructive. “The simplest neural network we could imagine is composed of a single linear function” known as a perceptron (Rosenblatt, Frank. *The perceptron, a perceiving and recognizing automaton Project Para.* Cornell Aeronautical Laboratory, 1957).
It is a network of perceptrons that reads the information in form of pixels.

**Ethics**

The rapid development of AI raises many questions all linking to how moral decisions are made. The moral machine is an online experimental platform, designed to look at the moral issues by gathering decisions made by millions of people from different locations with the aim to create a sort of “universal” moral preferences. Could we create a universal morality?
What if instead of creating a moral that is applicable everywhere and supposedly fair everywhere, we were to create a moral machine designed for each area? Could this work or would it promote bias and discrimination in certain locations?
Most importantly should this agent have as many rights as a human to take moral decisions in the face of a situation?

How do we define the moral codes in society, knowing that social constructs inform those norms/codes/laws/behaviours?

Robustness
Preventing risk
Self regulating recovery

Our physical world dictates how ones and zeros (binary system) are played as part of the game. For instance, giving the wrong information as a way to hack something.
This makes robustness difficult to assure.

Bias
*“A cognitive bias is a systematic pattern of deviation from norm or rationality in judgment. Individuals create their own "subjective social reality" from their perception of the input.”* (Bless, H.; Fiedler, K. & Strack, F. (2004). Social cognition: How individuals construct social reality. Hove and New York: Psychology Press.)
How to create unbiased machines?

We are perpetuating *bad* habits and behaviour in society such as discrimination, therefore AIs should be *better* humans. But how to if we teach a machine the same way we would teach a human? Should a machine be taught like a child? When does a child know it is part of something bigger?
To make machines learn the way humans have been learning could be a method to construct an AI’s algorithm independently from its actions.

Humans are too selfish not to try to connect with a superior version of what humans have become today.

Aren’t we going too fast? Shouldn’t we try to address those issues socially before trying to design those machines. If it is a fact that bias is always going to be there, how can we outweigh it? Why is bias always going to be there?

Political control
AI as a tool used by law to keep control over the social construct but also of the economic and political system.
What I find is on of a fundamental problem or gap in this is that those machines are missing the context awareness. If they were to have that context, then why not have a human instead?


### Data protection

Data is everywhere and its movement is faster than what we imagine. Data is constantly being shared, sold, bought unbeknownst to us. It is a trade that will only increase in the future, using anyone’s data to predict and manipulate behaviour in a way that will promote economic growth.

How can I protect my data? How much am I in control of it? Is it still mine to control?

Being an Iphone user, I looked into the operating system of Apple (IOS) and what sort of measures they take concerning data protection. It appears that Apple has always valued data protection mostly by building [encryption IOS into device’s hardware](https://searchmobilecomputing.techtarget.com/tip/How-iOS-encryption-and-data-protection-work). However I do not feel more in control of where it goes and what it becomes once I decide to use an online platform.
IOS encryption provides little in the way of real protection, other than to facilitate a fast, secure wipe of the system. This is an important feature, especially if a device is lost or stolen and a “remote wipe” has been configured beforehand.


**Smart city**

The social credit system in China was launched in 2014, with today about 500 smart city pilots. “The smart city is seen as the solution to many urban problems, including crime, traffic congestion, inefficient services and economic stagnation, promising prosperity and healthy lifestyles for all.”* (Escape The Smart City pdf) However, in leaves a lot of questions concerning privacy.
*”The opaque and incomprehensible nature of decision making by AI is often referred to as the “black box”. Once a machine learning model is trained, it can be difficult to understand why it gives a particular output to a set of data inputs. Even the developers can not explain why the AI arrived at a specific decision. The fact that these algorithms can act in ways unforeseen by their developer raises questions about the ‘decision-making,’ and ‘responsibility’ capacities of AI. (Mittelstadt, et al., 2016).”* (Escape The Smart City pdf)
How can we make AI more accessible and understood by society so that it doesn’t stay as this “black box” to some aspect?


### Kyle Mcdonald

A philosopher working in computer science, art and music.

Diagram deep learning < machine learning < AI.

The lecture started with the question of what is intelligence. It was interesting his definition of it as we had the chance to try define it in class at the beginning of the week.
Intelligence is often referred to as the IQ, which is, in average, a hundred for an individual. IN comparison, AI IQ could be a hundred time higher, and that is only for now. To come back to intelligence, IQ is actually misleading the definition of it. Intelligence can not be represented only by a number and there’s many things that could be seen as various types of intelligences.
Who is responsible for our intelligence? Who defines it? Can I design my own intelligence? In biology we could already explore the transformation of human skills which could be seen as redefining one side of intelligence.

“I am real person” vs. “ I am not a robot” situation. Who to trust?

Project with Liam Young “Exhausting a Crowd”, acts as an authority, watching everything.
I also enjoyed the narrative aspect to this project. How to make a story out of data?
A machine is not yet capable of doing that. What is it capable of now?
When is human authorship is non-essential?

We shouldn’t think that because we try things, it needs to be automated. But instead think of what human characteristics should be automated? What happens when computer choose who we are?


----------------------------------


### References

[*”Escape The Smart City”*](file:///C:/Users/julia/Downloads/Escape_the_Smart_City.pdf)

[BigML.com](https://bigml.com)

[Wekinator](http://www.wekinator.org)

[Kyle McDonald](http://www.kylemcdonald.net)

[Encryption IOS built into device’s hardware](https://searchsecurity.techtarget.com/definition/Advanced-Encryption-Standard)

Simon, Herbert (1976), Administrative Behavior (3rd ed.), New York: The Free Press

[Noam Chomsky on human intelligence and evironment](https://isreview.org/issue/76/human-intelligence-and-environment)

[Common Lisp](https://en.wikipedia.org/wiki/Common_Lisp)

Smith, Brian C (1982-01-01). *“Procedural Reflection In Programming Languages”*. MIT Technical Report (MIT-LCS–TR-272). Retrieved 16 December 2013.

Kumar, Anish and Babcock, Joseph (2017). *”Python: Advanced Predictive Analytics”*

[Sunspring (2016)](https://www.youtube.com/watch?v=LY7x2Ihqjmc)

2001: A Space Odyssey (1968) directed by Stanley Kubrick.

Floridi, Luciano (Ed.)(2015). *”The Onlife Manifesto: Being Human in a Hyperconnected Era”*

Bless, H.; Fiedler, K. & Strack, F. (2004). Social cognition: How individuals construct social reality. Hove and New York: Psychology Press.

[Liam Young](http://www.tomorrowsthoughtstoday.com/)

https://searchsecurity.techtarget.com/definition/Advanced-Encryption-Standard
